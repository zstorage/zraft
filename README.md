
zRaft由云和恩墨的分布式存储zStorage团队开发并维护，其构建于[canonical raft](https://github.com/canonical/raft)基础之上。

zStorage团队做了以下修改:

* 全流程异步处理改进, 新增raft_astart、raft_abootstrap等接口以及修改raft_fsm.apply为异步接口
* 支持joint consensus 配置变更
* 移植etcd raft的测试用例
* 新增logger角色
* 动态调节日志压缩速度
* 控制单次发送raft日志数量
* 增加回调接口监测raft运行指标

## Features
该实现包括 Raft 论文中描述的所有基本功能：

* 领导选举
* 日志复制
* 日志压缩
* 成员变更(单步以及joint consensus配置变更)

它还包括一些可选的增强功能：

* 优化流水线以减少日志复制的延迟
* 并行写入领导者的磁盘
* 领导者失去法定人数时自动下台
* 领导权转移扩展
* 预投票协议

## Building

从源码编译libraft，需要准备：

```bash
sudo apt-get install libuv1-dev liblz4-dev libtool pkg-config build-essential

autoreconf -i

./configure --enable-example

make
```

## Example

理解如何使用raft库的最好方式是阅读源码目录下的[example server](https://gitee.com/zstorage/zraft/tree/master/example/server.c)。

可以运行如下命令来了解example server的运行情况：

```bash
./example/cluster
```
