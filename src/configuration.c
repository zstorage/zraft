#include "configuration.h"

#include "assert.h"
#include "event.h"
#include "byte.h"

/* Current encoding format version. */
#define ENCODING_FORMAT 1
#define CONF_SERVER_VERSION_V1 1
#define CONF_SERVER_VERSION_V2 2
/**
 * raft_configuration meta info, fixed size 256.
 */
struct raft_configuration_meta
{
    uint32_t version;
    uint32_t server_version;
    uint32_t server_size;
    uint8_t  phase;
    uint8_t  reserve[243];
};

void configurationInit(struct raft_configuration *c)
{
    c->servers = NULL;
    c->n = 0;
    c->phase = RAFT_CONF_NORMAL;
}

void configurationClose(struct raft_configuration *c)
{
    size_t i;
    assert(c != NULL);
    assert(c->n == 0 || c->servers != NULL);

    for (i = 0; i < c->n; i++) {
        raft_free(c->servers[i].address);
    }
    if (c->servers != NULL) {
        raft_free(c->servers);
        c->servers = NULL;
    }
}

unsigned configurationIndexOf(const struct raft_configuration *c,
                              const raft_id id)
{
    unsigned i;
    assert(c != NULL);
    for (i = 0; i < c->n; i++) {
        if (c->servers[i].id == id) {
            return i;
        }
    }
    return c->n;
}

bool configurationIsVoter(const struct raft_configuration *c,
                          const struct raft_server *s, int group)
{
    bool voter = false;

    if (s->group & RAFT_GROUP_OLD & group)
        voter = (s->role == RAFT_VOTER || s->role == RAFT_LOGGER);

    if (s->group & RAFT_GROUP_NEW & group) {
        assert(c->phase == RAFT_CONF_JOINT);
        voter = voter || (s->role_new == RAFT_VOTER
                            || s->role_new == RAFT_LOGGER);
    }

    return voter;
}

bool configurationIsSpare(const struct raft_configuration *c,
                          const struct raft_server *s, int group)
{
    bool spare = false;

    if (s->group & RAFT_GROUP_OLD & group)
        spare = s->role == RAFT_SPARE;

    if (s->group & RAFT_GROUP_NEW & group) {
        assert(c->phase == RAFT_CONF_JOINT);
        spare = spare && (s->role_new == RAFT_SPARE);
    }

    return spare;
}

int configurationJointToNormal(const struct raft_configuration *src,
                               struct raft_configuration *dst,
                               enum raft_group group)
{
    size_t i;
    int rv;
    int role;

    assert(group == RAFT_GROUP_OLD || group == RAFT_GROUP_NEW);
    assert(src->phase == RAFT_CONF_JOINT);
    configurationInit(dst);
    for (i = 0; i < src->n; i++) {
        struct raft_server *server = &src->servers[i];
        if (!(server->group & (int)group))
            continue;
        role = (group == RAFT_GROUP_OLD) ? server->role: server->role_new;
        rv = configurationAdd(dst, server->id, server->address, role, role, RAFT_GROUP_OLD);
        if (rv != 0) {
            evtErrf("E-1528-106", "add conf failed id %d role %d", server->id, server->role);
            return rv;
        }
    }
    dst->phase = RAFT_CONF_NORMAL;
    return 0;
}



unsigned configurationIndexOfVoter(const struct raft_configuration *c,
                                   const raft_id id)
{
    unsigned i;
    unsigned j = 0;
    assert(c != NULL);
    assert(id > 0);

    for (i = 0; i < c->n; i++) {
        if (c->servers[i].id == id) {
            if (configurationIsVoter(c, &c->servers[i], RAFT_GROUP_ANY)) {
                return j;
            }
            return c->n;
        }

        if (configurationIsVoter(c, &c->servers[i], RAFT_GROUP_ANY)) {
            j++;
        }
    }

    return c->n;
}

const struct raft_server *configurationGet(const struct raft_configuration *c,
                                           const raft_id id)
{
    size_t i;
    assert(c != NULL);
    assert(id > 0);

    /* Grab the index of the server with the given ID */
    i = configurationIndexOf(c, id);

    if (i == c->n) {
        /* No server with matching ID. */
        return NULL;
    }
    assert(i < c->n);

    return &c->servers[i];
}

unsigned configurationVoterCount(const struct raft_configuration *c, int group)
{
    unsigned i;
    unsigned n = 0;
    assert(c != NULL);

    for (i = 0; i < c->n; i++) {
        if (configurationIsVoter(c, &c->servers[i], group)) {
            n++;
        }
    }
    return n;
}

int configurationCopy(const struct raft_configuration *src,
                      struct raft_configuration *dst)
{
    size_t i;
    int rv;
    configurationInit(dst);
    for (i = 0; i < src->n; i++) {
        struct raft_server *server = &src->servers[i];
        rv = configurationAdd(dst, server->id, server->address, server->role,
                              server->role_new, server->group);
        if (rv != 0) {
            evtErrf("E-1528-107", "add conf failed id %d role %d", server->id, server->role);
            return rv;
        }
    }
    dst->phase = src->phase;
    return 0;
}

static int configurationUpdateAddress(struct raft_configuration *c,
                                      const raft_id id,
                                      const char *address)
{
    size_t i;
    char *address_copy;
    const struct raft_server *server;
    assert(c != NULL);
    assert(id != 0);
    assert(address);

    for (i = 0; i < c->n; i++) {
        server = &c->servers[i];
        if (server->address && address && !strcmp(server->address, address)) {
            evtErrf("E-1528-274", "conf update duplicated address %s", address);
            return RAFT_DUPLICATEADDRESS;
        }
    }

    /* Make a copy of the given address */
    address_copy = raft_malloc(strlen(address) + 1);
    if (address_copy == NULL) {
        return RAFT_NOMEM;
    }
    strcpy(address_copy, address);

    server = configurationGet(c, id);
    if (server == NULL) {
        evtErrf("E-1528-275", "conf update invalid id %lx", id);
        raft_free(address_copy);
        return RAFT_BADID;
    }

    raft_free(server->address);
    ((struct raft_server *)server)->address = address_copy;
    return 0;
}

int configurationAdd(struct raft_configuration *c,
                     raft_id id,
                     const char *address,
                     int role,
                     int role_new,
                     int group)
{
    struct raft_server *servers;
    struct raft_server *server;
    char *address_copy;
    size_t i;
    assert(c != NULL);
    assert(id != 0);

    if (role != RAFT_STANDBY && role != RAFT_VOTER && role != RAFT_SPARE && role != RAFT_LOGGER) {
        evtErrf("E-1528-108", "conf add bad role %d", role);
        return RAFT_BADROLE;
    }

    /* Check that neither the given id or address is already in use */
    for (i = 0; i < c->n; i++) {
        server = &c->servers[i];
        if (server->id == id) {
            evtErrf("E-1528-109", "conf add duplicated id %llx", id);
            return RAFT_DUPLICATEID;
        }

        if (server->address && address && !strcmp(server->address, address)) {
            evtErrf("E-1528-273", "conf add duplicated address %s", address);
            return RAFT_DUPLICATEADDRESS;
        }
    }

    if (address) {
        /* Make a copy of the given address */
        address_copy = raft_malloc(strlen(address) + 1);
        if (address_copy == NULL) {
            return RAFT_NOMEM;
        }
        strcpy(address_copy, address);
    } else {
        address_copy = NULL;
    }

    /* Grow the servers array.. */
    servers = raft_realloc(c->servers, (c->n + 1) * sizeof *server);
    if (servers == NULL) {
        evtErrf("E-1528-110", "conf add realloc failed, id %llx role %d", id, role);
        raft_free(address_copy);
        return RAFT_NOMEM;
    }
    c->servers = servers;

    /* Fill the newly allocated slot (the last one) with the given details. */
    server = &servers[c->n];
    server->id = id;
    server->address = address_copy;
    server->role = role;
    server->role_new = role_new;
    server->group = group;
    c->n++;

    return 0;
}

int configurationRemove(struct raft_configuration *c, const raft_id id)
{
    unsigned i;
    unsigned j;
    struct raft_server *servers;
    assert(c != NULL);

    i = configurationIndexOf(c, id);
    if (i == c->n) {
        evtErrf("E-1528-111", "conf remove bad id %llx", id);
        return RAFT_BADID;
    }

    assert(i < c->n);

    /* If this is the last server in the configuration, reset everything. */
    if (c->n - 1 == 0) {
        assert(i == 0);
        servers = NULL;
        goto out;
    }

    /* Create a new servers array. */
    servers = raft_calloc(c->n - 1, sizeof *servers);
    if (servers == NULL) {
        evtErrf("E-1528-112", "conf remove calloc failed, id %llx", id);
        return RAFT_NOMEM;
    }

    /* Copy the first part of the servers array into a new array, excluding the
     * i'th server. */
    for (j = 0; j < i; j++) {
        servers[j] = c->servers[j];
    }

    /* Copy the second part of the servers array into a new array. */
    for (j = i + 1; j < c->n; j++) {
        servers[j - 1] = c->servers[j];
    }

 out:
    /* Release the address of the server that was deleted. */
    raft_free(c->servers[i].address);

   /* Release the old servers array */
    raft_free(c->servers);

    c->servers = servers;
    c->n--;

    return 0;
}

void configurationJointRemove(struct raft_configuration *c, raft_id id)
{
    assert(c->phase == RAFT_CONF_NORMAL);
    size_t i;

    for (i = 0; i < c->n; ++i) {
        if (c->servers[i].id == id) {
            c->servers[i].group = RAFT_GROUP_OLD;
            continue;
        }
        c->servers[i].group = RAFT_GROUP_OLD | RAFT_GROUP_NEW;
        c->servers[i].role_new = c->servers[i].role;
    }
    c->phase = RAFT_CONF_JOINT;
}

void configurationJointReset(struct raft_configuration *c)
{
    assert(c->phase == RAFT_CONF_JOINT);
    size_t i;

    for (i = 0; i < c->n; ++i) {
        c->servers[i].group = RAFT_GROUP_OLD;
    }
    c->phase = RAFT_CONF_NORMAL;
}

size_t configurationEncodedSize(const struct raft_configuration *c)
{
    size_t n = 0;
    unsigned i;
    struct raft_server *server;

    /* We need one byte for the encoding format version */
    n++;
    /* Then 8 bytes for number of servers. */
    n += sizeof(uint64_t);

    /* Then some space for each server. */
    for (i = 0; i < c->n; i++) {
        n += sizeof(uint64_t);            /* Server ID */
        n++;                              /* Voting flag */
    };

    n += sizeof(struct raft_configuration_meta);
    /* Then some space for each server. */
    for (i = 0; i < c->n; i++) {
        server = &c->servers[i];
        n += sizeof(uint64_t);            /* Server ID */
        n++;                              /* Voting flag */
        n += sizeof(uint16_t);            /* New group role and group */
        /* Address */
        if (server->address) {
            n += strlen(server->address) + 1;
        } else {
            n += 1;
        }
    };

    return bytePad64(n);
}

void configurationEncodeToBuf(const struct raft_configuration *c, void *buf)
{
    void *cursor = buf;
    unsigned i;
    struct raft_configuration_meta meta = {0};
    uint8_t *start;
    struct raft_server *server;

    assert(sizeof(meta) == CONF_META_SIZE);
    /* Encoding format version */
    bytePut8(&cursor, ENCODING_FORMAT);
    /* Number of servers. */
    bytePut64Unaligned(&cursor, c->n); /* cursor might not be 8-byte aligned */

    for (i = 0; i < c->n; i++) {
        server = &c->servers[i];
        bytePut64Unaligned(&cursor, server->id); /* might not be aligned */
        assert(server->role < 255);
        bytePut8(&cursor, (uint8_t)server->role);
    };

    meta.version = CONF_META_VERSION;
    meta.server_version = CONF_SERVER_VERSION;
    meta.server_size = CONF_SERVER_SIZE;
    meta.phase = c->phase;
    bytePut32(&cursor, meta.version);
    bytePut32(&cursor, meta.server_version);
    bytePut32(&cursor, meta.server_size);
    bytePut8(&cursor, meta.phase);
    for (i = 0; i < sizeof(meta.reserve); i++){
        bytePut8(&cursor, meta.reserve[i]);
    }

    for (i = 0;i < c->n; i++) {
        server = &c->servers[i];
        start = (uint8_t *)cursor;
        bytePut64Unaligned(&cursor, server->id); /* might not be aligned */
        assert(server->role < 255);
        bytePut8(&cursor, (uint8_t)server->role);
        bytePut8(&cursor, (uint8_t)server->role_new);
        bytePut8(&cursor, (uint8_t)server->group);
        assert(((uint8_t *)cursor - start) == CONF_SERVER_SIZE);
    }

    for (i = 0; i < c->n; i++) {
        server = &c->servers[i];
        if (server->address) {
            bytePutString(&cursor, server->address);
        } else {
            bytePut8(&cursor, 0);
        }
    }
}

static size_t diffPtr(const void *start, const void *end)
{
    assert((uint8_t *)end  >= (uint8_t *)start);

    return (uintptr_t)(uint8_t *)end - (uintptr_t)(uint8_t *)start;
}

int configurationDecodeFromBuf(const void *buf, struct raft_configuration *c,
                               size_t size)
{
    size_t i;
    size_t n;
    int rv;
    int role;
    raft_id id;
    const char *address;
    const void *start = buf;
    struct raft_server *s;
    struct raft_configuration_meta meta = {0};

    /* Check the encoding format version */
    if (byteGet8(&buf) != ENCODING_FORMAT) {
        evtErrf("E-1528-113", "%s", "malformed");
	    return RAFT_MALFORMED;
    }
    /* then read the phase */
    c->phase = RAFT_CONF_NORMAL;
    /* Read the number of servers. */
    n = (size_t)byteGet64Unaligned(&buf);

    /* Decode the individual servers. */
    for (i = 0; i < n; i++) {
        /* Server ID. */
        id = byteGet64Unaligned(&buf);
        /* Role code. */
        role = byteGet8(&buf);
        rv = configurationAdd(c, id, NULL, role, role, RAFT_GROUP_OLD);
        if (rv != 0) {
            evtErrf("E-1528-114", "conf add %llx failed", id, rv);
            goto err;
        }
        assert(configurationIndexOf(c, id) == i);
    }

    if (size < diffPtr(start, buf) + CONF_META_SIZE)
        return 0;

    meta.version = byteGet32(&buf);
    meta.server_version = byteGet32(&buf);
    meta.server_size = byteGet32(&buf);
    meta.phase = byteGet8(&buf);
    // discard reserve filed
    buf = (const uint8_t *)buf + sizeof(meta.reserve);

    assert(meta.server_version >= CONF_SERVER_VERSION_V1);
    assert(meta.server_size == CONF_SERVER_SIZE);
    for (i = 0; i < n; i++) {
        /* Server ID. */
        id = byteGet64Unaligned(&buf);
        /* Role code. */
        role = byteGet8(&buf);
        s = (struct raft_server *)configurationGet(c, id);
        assert(s);
        assert(s->role == role);
        /* New role */
        s->role_new = byteGet8(&buf);
        /* Group */
        s->group = byteGet8(&buf);
    }
    c->phase = meta.phase;

    if (meta.server_version < CONF_SERVER_VERSION_V2) {
        goto out;
    }

    for (i = 0; i < n; i++) {
        address = byteGetString(&buf, size - (size_t)((char *)buf - (char *)start));
        if (address == NULL) {
            rv = RAFT_MALFORMED;
            goto err;
        }
        if (strlen(address) == 0) {
            continue;
        }
        rv = configurationUpdateAddress(c, c->servers[i].id, address);
        if (rv != 0) {
            goto err;
        }
    }
out:
    return 0;
err:
    configurationClose(c);
    assert(rv);
    return rv;

}
static int defaultEncode(void *ptr,
			 const struct raft_configuration *c,
			 struct raft_buffer *buf)
{
    (void)ptr;
    assert(c != NULL);
    assert(buf != NULL);

    /* The configuration can't be empty. */
    assert(c->n > 0);

    buf->len = configurationEncodedSize(c);
    buf->base = raft_entry_malloc(buf->len);
    if (buf->base == NULL) {
        evtErrf("E-1528-115", "%s", "malloc failed");
        return RAFT_NOMEM;
    }

    configurationEncodeToBuf(c, buf->base);

    return 0;
}

static int defaultDecode(void *ptr,
			 const struct raft_buffer *buf,
			 struct raft_configuration *c)
{
    (void)ptr;


    assert(c != NULL);
    assert(buf != NULL);

    /* TODO: use 'if' instead of assert for checking buffer boundaries */
    assert(buf->len > 0);

    /* Check that the target configuration is empty. */
    assert(c->n == 0);
    assert(c->servers == NULL);
    return configurationDecodeFromBuf(buf->base, c, buf->len);
}

static struct raft_configuration_codec defaultCodec = {
	NULL,
	defaultEncode,
	defaultDecode,
};

static struct raft_configuration_codec *currentCodec = &defaultCodec;

int configurationEncode(const struct raft_configuration *c,
			struct raft_buffer *buf)
{
	return currentCodec->encode(currentCodec->data, c, buf);
}

int configurationDecode(const struct raft_buffer *buf,
			struct raft_configuration *c)
{
	return currentCodec->decode(currentCodec->data, buf, c);
}


void raft_configuration_codec_set(struct raft_configuration_codec *codec)
{
	currentCodec = codec;
}

void raft_configuration_codec_set_default(void)
{
	currentCodec = &defaultCodec;
}

int configurationServerRole(const struct raft_configuration *c, raft_id id)
{
    const struct raft_server *server = configurationGet(c, id);

    assert(server);
    if (server->group & RAFT_GROUP_NEW)
        return server->role_new;
    return server->role;
}

bool configurationHasRole(const struct raft_configuration *c, int role)
{
    unsigned i;
    struct raft_server *s;

    for (i = 0; i < c->n; ++i) {
        s = &c->servers[i];
        if (configurationServerRole(c, s->id) == role)
            return true;
    }
    return false;
}

static const char *roleNames[] = {"stand-by", "voter", "spare", "logger"};
const char *configurationRoleName(int role)
{
    assert(role == RAFT_STANDBY || role == RAFT_VOTER || role == RAFT_SPARE
        || role == RAFT_LOGGER);
    return roleNames[role];
}

static const char *phaseNames[] =  {"normal", "joint"};
const char *configurationPhaseName(int phase)
{
    assert(phase == RAFT_CONF_NORMAL || phase == RAFT_CONF_JOINT);
    return phaseNames[phase];
}

static const char *groupNames[] = {"", "old", "new", "both"};
const char *configurationGroupName(int group)
{
    assert(group == RAFT_GROUP_OLD || group == RAFT_GROUP_NEW
        || group == RAFT_GROUP_ANY);
    return groupNames[group];
}
